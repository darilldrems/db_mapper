<?php
	 /**
        *@author Ridwan Olalere
        *
        */
        class Mapper{

                public $json_object;
                public $table_name;

                public function __construct($table_name){
                        $this->table_name = $table_name;
                        $this->loadData();      
                }
                

                private function loadData(){
			$content = file_get_contents('./json_files/'.$this->table_name.'.json', true);
			if($content === False){
				throw new Exception("Table name does not exists");
			}                        
			
			$this->json_object = json_decode($content);
			if($this->json_object == NULL)
				throw new Exception("JSON file probably has an error in it");		
                }


                /**
                *@param $column_name table column name
                *
                *@return {String} returns field name to be used in json result
                */
                public function getFieldName($column_name){
  
			
                      foreach($this->json_object as $key => $value){
				if($key === $column_name)
					return $value;
			}
			return false;
                }

		public function getColumnName($field_name){
			foreach($this->json_object as $key => $value){
                                if($value === $column_name){
                                        return $key;
                                }
                        }
                        return false;
		}

	}

	$test = new Mapper('user');
	echo $test->getFieldName('email_address');

?>
